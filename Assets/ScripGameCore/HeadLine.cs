﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HeadLine : MonoBehaviour
{
    public bool wasTest;
    private void Start()
    {
        wasTest = false;
    }
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if(collision.gameObject.tag != "BirdCheck")
       if(!wasTest)
        {
            Debug.LogError("collision " + collision.gameObject.name);
            LinesDrawer.Instance.EndDraw();
            wasTest = true;
        }
      
    }
    //private void OnCollisionEnter2D(Collision2D collision)
    //{
        
    //        Debug.LogError("collision " + collision.gameObject.name);
    //}
}
