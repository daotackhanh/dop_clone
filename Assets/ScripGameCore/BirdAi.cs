﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using Spine.Unity;

public class BirdAi : MonoBehaviour
{
    private const string FLY = "GAMEPLAY/fly";
    private const string IDLE_0 = "GAMEPLAY/idle";
    private const string IDLE_1 = "HOMEDECOR/idle";
    private const string IDLE_2 = "HOMEDECOR/idle2";
    private const string IDLE_3 = "HOMEDECOR/idle3";
    private const string IDLE_4 = "HAIR/IDLE";
    private const string IDLE_3_1 = "HOMEDECOR/idle3_1"; //?ANIM ĐÁP ĐẤT
    private const string WALK = "HOMEDECOR/walk";
    private const string JUMP = "HOMEDECOR/jump";
    private const string CLEAR = "HAIR/CLEAR";
    private const string BRUSHING = "HAIR/BRUSHING";
    public List<Transform> lsTranform;
    public SkeletonAnimation skeletonAnimation;
   
    private void Start()
    {
        SetUp();
    }

    private void Update()
    {
     
    }


    private void SetUp()
    {       
        this.transform.position = lsTranform[0].position;
        Move();
    }

    private void Move()
    {
        skeletonAnimation.SetAnimation(IDLE_0,false);
        var tempRan = Random.Range(0, lsTranform.Count);
        var timeRan = Random.Range(2, 5);
        if (lsTranform[tempRan].transform.position.x > this.transform.position.x)
        {
            this.transform.localScale = new Vector3(1, 1, 1);
        }
        if (lsTranform[tempRan].transform.position.x < this.transform.position.x)
        {
            this.transform.localScale = new Vector3(-1, 1, 1);
        }
        this.transform.DOMove(lsTranform[tempRan].position, timeRan).OnComplete(
        delegate
        {
            Move();


        }).OnUpdate(delegate { skeletonAnimation.SetAnimation(FLY,false); });

    }
  
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if(collision.gameObject.tag == "Line")
        {
            LinesDrawer.Instance.EndDraw();
        }
    }
}
